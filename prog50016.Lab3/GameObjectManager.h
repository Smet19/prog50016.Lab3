#pragma once
#ifndef _GAMEOBJECTMANAGER_H_
#define	_GAMEOBJECTMANAGER_H_

#include "Singleton.h"
#include "ISystem.h"
#include "GameObject.h"
#include <list>

class GameObjectManager :  public ISystem
{
public:
	void AddGameObject(GameObject* component) { gameObjects.push_back(component); }
	void RemoveGameObject(GameObject* component) { gameObjects.remove(component); }
	GameObject* FindGameObjectById(int id);

protected:
	virtual void Initialize() override {}
	virtual void Update() override {}

private:
	friend class GameEngine;
	std::list<GameObject*> gameObjects;

	MEYERS_SINGLETON_CUSTOM(GameObjectManager)
};

#endif // !_GAMEOBJECTMANAGER_H_

