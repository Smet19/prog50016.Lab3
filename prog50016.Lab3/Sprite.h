#pragma once
#ifndef _SPRITE_H_
#define _SPRITE_H_

#include "Component.h"
#include "IRenderable.h"

class Sprite : public Component, public IRenderable
{
public:
	Sprite() { componentId = "DefaultID"; };

	virtual std::string& GetComponentId() override { return componentId; }

private:
	std::string componentId;
};

#endif // !_SPRITE_H_