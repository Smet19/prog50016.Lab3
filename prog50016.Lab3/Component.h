#pragma once
#ifndef _COMPONENT_H_
#define	_COMPONENT_H_

#include "Object.h"

class Component : public Object
{
protected:
	Component();
	virtual ~Component();

public:
	virtual void Initialize() override;
	virtual void Update();
	virtual std::string& GetComponentId() = 0;
};

#endif // !_COMPONENT_H_

