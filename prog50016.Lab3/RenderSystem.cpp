#include "RenderSystem.h"
#include "json.hpp"
#include <fstream>

RenderSystem::RenderSystem()
{
	name = "DefaultName";
	width = 640;
	height = 480;
	fullscreen = false;
}

RenderSystem::~RenderSystem()
{
	for (auto* comp : renderComponents)
	{
		delete comp;
	}
}

void RenderSystem::LoadSettings()
{
	// Load JSON file
	std::ifstream inputStream("./Assets/RenderSystem.json");
	std::string str((std::istreambuf_iterator<char>(inputStream)), std::istreambuf_iterator<char>());
	json::JSON renderSystemJSON = json::JSON::Load(str);

	if (renderSystemJSON.hasKey("name"))
	{
		name = renderSystemJSON["name"].ToString();
	}

	if (renderSystemJSON.hasKey("width"))
	{
		// Every value in provided JSON file is string, I'm not sure if it's intentional
		// But my code assumes it is intentional
		// In case it's not - uncomment commented lines, comment the other ones
		
		//width = renderSystemJSON["width"].ToInt();
		width = std::stoi(renderSystemJSON["width"].ToString());
	}

	if (renderSystemJSON.hasKey("height"))
	{
		//height = renderSystemJSON["height"].ToInt();
		height = std::stoi(renderSystemJSON["height"].ToString());
	}

	if (renderSystemJSON.hasKey("fullscreen"))
	{
		//fullscreen = renderSystemJSON["fullscreen"].ToBool();
		fullscreen = std::stoi(renderSystemJSON["fullscreen"].ToString());
	}
}
