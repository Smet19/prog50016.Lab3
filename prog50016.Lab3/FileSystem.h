#pragma once
#ifndef _FILESYSTEM_H_
#define	_FILESYSTEM_H_

#include "Singleton.h"
#include "ISystem.h"
#include <string>

class FileSystem :  public ISystem
{
protected:
	virtual void Initialize() override {}
	virtual void Update() override {}
	virtual void Load(std::string fileName) {}

private:
	friend class GameEngine;

	MEYERS_SINGLETON(FileSystem)
};

#endif // !_FILESYSTEM_H_