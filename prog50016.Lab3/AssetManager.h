#pragma once
#ifndef _ASSETMANAGER_H_
#define	_ASSETMANAGER_H_

#include "Singleton.h"
#include "ISystem.h"
#include "Asset.h"
#include <list>

class AssetManager :  public ISystem
{
public:
	void AddAsset(Asset* component) { assets.push_back(component); }
	void RemoveAsset(Asset* component) { assets.remove(component); }

protected:
	virtual void Initialize() override {}
	virtual void Update() override {}

private:
	friend class GameEngine;
	std::list<Asset*> assets;

	MEYERS_SINGLETON_CUSTOM(AssetManager)
};
#endif // !_ASSETMANAGER_H_

