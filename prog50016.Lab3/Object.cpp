#include "Object.h"

Object::Object() 
{
	initialized = false;
	name = "DefaultName";
	id = 0;
}

void Object::Initialize()
{
	if (!initialized)
	{
		initialized = true;
	}
}
