#pragma once
#ifndef _RENDERSYSTEM_H_
#define	_RENDERSYSTEM_H_

#include "Singleton.h"
#include "ISystem.h"
#include "IRenderable.h"
#include <string>
#include <list>

class RenderSystem :  public ISystem
{

public:
	void AddRenderable(IRenderable* component) { renderComponents.push_back(component); }
	void RemoveRenderable(IRenderable* component) { renderComponents.remove(component); };

protected:
	virtual void Update() override {};
	virtual void Initialize() override { LoadSettings(); };

private:
	friend class GameEngine;

	std::list<IRenderable*> renderComponents;
	std::string name;
	int width;
	int height;
	bool fullscreen;

	void LoadSettings();

	MEYERS_SINGLETON_CUSTOM(RenderSystem)
};
#endif // !_RENDERSYSTEM_H_

