#include "GameObjectManager.h"

GameObjectManager::GameObjectManager()
{
}

GameObjectManager::~GameObjectManager()
{
	for (auto* object : gameObjects)
	{
		delete object;
	}
}

GameObject* GameObjectManager::FindGameObjectById(int id)
{
	// There might be a better way to do that
	for (auto* object : gameObjects)
	{
		if (object->GetId() == id)
			return object;
	}
	return nullptr;
}