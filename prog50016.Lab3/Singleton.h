#pragma once
#ifndef _SINGLETON_H_
#define _SINGLETON_H_

template <typename T>
class Singleton
{
public:
	static T& Instance()
	{
		static T instance;
		return instance;
	}

protected:
	inline explicit Singleton() = default;
	inline ~Singleton() = default;

private:
	inline explicit Singleton(Singleton const&) = delete;
	inline Singleton& operator=(Singleton const&) = delete;
};

#define MEYERS_SINGLETON(_Singleton_) \
	public: \
		inline static _Singleton_& Instance()\
		{\
			static _Singleton_ instance;\
			return instance;\
		}\
	\
	private:\
		inline explicit _Singleton_() = default;\
		inline ~_Singleton_() = default;\
		inline explicit _Singleton_(_Singleton_ const&) = delete;\
		inline _Singleton_& operator=(_Singleton_ const&) = delete;

#define MEYERS_SINGLETON_CUSTOM(_Singleton_) \
	public: \
		inline static _Singleton_& Instance()\
		{\
			static _Singleton_ instance;\
			return instance;\
		}\
	\
	private:\
		_Singleton_();\
		~_Singleton_();\
		inline explicit _Singleton_(_Singleton_ const&) = delete;\
		inline _Singleton_& operator=(_Singleton_ const&) = delete;
#endif // !_SINGLETON_H_