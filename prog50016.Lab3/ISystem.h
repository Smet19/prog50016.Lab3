#pragma once
#ifndef _I_SYSTEM_H_
#define _I_SYSTEM_H_

class ISystem
{
	friend class GameEngine;

protected:
	virtual void Initialize() = 0;
	virtual void Update() = 0;
};

#endif // !_I_SYSTEM_H_

