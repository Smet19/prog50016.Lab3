#include "GameEngine.h"
#include "RenderSystem.h"
#include "FileSystem.h"
#include "InputManager.h"
#include "AssetManager.h"
#include "GameObjectManager.h"

void GameEngine::Initialize()
{
	FileSystem::Instance().Initialize();
	InputManager::Instance().Initialize();
	AssetManager::Instance().Initialize();
	GameObjectManager::Instance().Initialize();
	RenderSystem::Instance().Initialize();
}

void GameEngine::GameLoop()
{
	FileSystem::Instance().Update();
	InputManager::Instance().Update();
	AssetManager::Instance().Update();
	GameObjectManager::Instance().Update();
	RenderSystem::Instance().Update();
}