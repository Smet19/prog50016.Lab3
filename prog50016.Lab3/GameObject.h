#pragma once
#ifndef _GAMEOBJECT_H_
#define	_GAMEOBJECT_H_

#include <map>
#include "Component.h"

class GameObject : public Object
{
public:
	GameObject() {};
	virtual ~GameObject() {};

	void AddComponent(Component* _component) { components.emplace(_component->GetComponentId(), _component); }
	void RemoveComponent(Component* _component) { components.erase(_component->GetComponentId()); }
	virtual void Update() {};
	virtual void Initialize() override {};

private:
	std::map<std::string, Component*> components;
};

#endif // !_GAMEOBJECT_H_