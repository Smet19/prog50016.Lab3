#pragma once
#ifndef _I_RENDERABLE_H_
#define _I_RENDERABLE_H_

class IRenderable
{
	friend class RenderSystem;

protected:
	IRenderable() {};
	virtual ~IRenderable() {};

	virtual void Render() = 0;
};

#endif // !_I_RENDERABLE_H_