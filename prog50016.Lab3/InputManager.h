#pragma once
#ifndef _INPUTMANAGER_H_
#define	_INPUTMANAGER_H_

#include "Singleton.h"
#include "ISystem.h"

class InputManager :  public ISystem
{
protected:
	virtual void Initialize() override {}
	virtual void Update() override {}

private:
	friend class GameEngine;

	MEYERS_SINGLETON(InputManager)
};

#endif // !_INPUTMANAGER_H_

