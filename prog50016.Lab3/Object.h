#pragma once
#ifndef _OBJECT_H_
#define _OBJECT_H_

#include "Asset.h"
#include <string>

class Object : public Asset
{
protected:
	Object();
	virtual ~Object() {};

public:
	bool isInitialized() { return initialized; }
	virtual void Initialize();
	std::string GetName() { return name; }
	int GetId() { return id; }

private:
	bool initialized;
	std::string name;
	int id;
};
#endif // !_OBJECT_H_