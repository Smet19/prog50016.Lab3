#pragma once
#ifndef _GAMEENGINE_H_
#define _GAMEENGINE_H_

#include "Singleton.h"
#include "ISystem.h"

class GameEngine 
{

public:
	void Initialize();
	void GameLoop();

	MEYERS_SINGLETON(GameEngine)
};
#endif // !_GAMEENGINE_H_